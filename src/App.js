import logo from './logo.svg';
import './App.css';
import {Status} from "./status/status";
import {Playground} from "./playground/playground";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Status></Status>
        <Playground></Playground>
      </header>
    </div>
  );
}

export default App;
